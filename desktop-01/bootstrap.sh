#!/bin/bash
#
# bootstrap.sh
echo "Configuring DNS."
{ echo '# /etc/resolv.conf'
  echo 'search sandbox.lan'
  echo 'nameserver 10.23.45.10'
} > /etc/resolv.conf
