# Sandbox network

This repository provides a collection of Vagrantfiles and bootstrap scripts for
building a complete Linux test environment:

  * Gateway

  * Server No. 1

  * Server No. 2

  * Desktop No. 1

  * Desktop No. 2


# Software requirements

This network of virtual machines requires VirtualBox and Vagrant. 

# Usage

1. Launch the gateway machine:

```
$ cd gateway
$ vagrant up
```

Once the gateway's up, start whatever box you need. 

2. Change into the corresponding directory: `cd server-01`

3. Start the box: `vagrant up`

# Virtual machine specifications

## Gateway

  * Hostname: `gateway.sandbox.lan`

  * System: CentOS 7.8 

  * IP address: 10.23.45.10

  * Dnsmasq server 

  * Gateway/router 
  
## Server No. 1

  * Hostname: `server-01.sandbox.lan`

  * System: CentOS 7.8

  * IP address: 10.23.45.20

## Server No. 2

  * Hostname: `server-02.sandbox.lan`

  * System: CentOS 7.8 

  * IP address: 10.23.45.30

## Desktop client No. 1

  * Hostname: `desktop-01.sandbox.lan`

  * System: OpenSUSE Leap 15.1

  * IP address: 10.23.45.40

## Desktop client No. 2

  * Hostname: `desktop-02.sandbox.lan`

  * Minimal OpenSUSE Leap 15.1 KDE desktop

  * IP address: 10.23.45.50



