#!/bin/bash
#
# bootstrap.sh
echo "Configuring firewall."
firewall-cmd --permanent --zone=external --change-interface=eth0 > /dev/null
firewall-cmd --permanent --zone=internal --change-interface=eth1 > /dev/null
firewall-cmd --set-default-zone=internal > /dev/null
firewall-cmd --permanent --remove-service=dhcpv6-client > /dev/null
firewall-cmd --permanent --remove-service=mdns > /dev/null
firewall-cmd --permanent --remove-service=samba-client > /dev/null
firewall-cmd --permanent --add-service=ntp > /dev/null
firewall-cmd --permanent --add-service=dhcp > /dev/null
firewall-cmd --permanent --add-service=dns > /dev/null
firewall-cmd --reload > /dev/null
echo "Configuring NTP server."
{ echo '# /etc/chrony.conf'
  echo 'server 0.fr.pool.ntp.org iburst'
  echo 'server 1.fr.pool.ntp.org iburst'
  echo 'server 2.fr.pool.ntp.org iburst'
  echo 'server 3.fr.pool.ntp.org iburst'
  echo 'driftfile /var/lib/chrony/drift'
  echo 'makestep 1.0 3'
  echo 'rtcsync'
  echo 'logdir /var/log/chrony'
} > /etc/chrony.conf
systemctl restart chronyd > /dev/null
echo "Configuring local hosts."
{ echo '# /etc/hosts'
  echo '127.0.0.1   localhost.localdomain localhost'
  echo '10.23.45.10 gateway.sandbox.lan gateway'
  echo '10.23.45.20 server-01.sandbox.lan server-01'
  echo '10.23.45.30 server-02.sandbox.lan server-02'
  echo '10.23.45.40 desktop-01.sandbox.lan desktop-01'
  echo '10.23.45.50 desktop-02.sandbox.lan desktop-02'
} > /etc/hosts
echo "Configuring DNS server."
yum install -y dnsmasq > /dev/null
{ echo '# /etc/dnsmasq.conf'
  echo 'domain-needed'
  echo 'bogus-priv'
  echo 'interface=eth1'
  echo 'dhcp-range=10.23.45.100,10.23.45.200,24h'
  echo 'local=/sandbox.lan/'
  echo 'domain=sandbox.lan'
  echo 'expand-hosts'
  echo 'server=1.1.1.1'
  echo 'server=1.0.0.1'
  echo 'no-resolv'
  echo 'log-facility=/var/log/dnsmasq.log'
} > /etc/dnsmasq.conf
systemctl enable dnsmasq --now > /dev/null 2>&1
{ echo '# /etc/resolv.conf'
  echo 'nameserver 127.0.0.1'
} > /etc/resolv.conf
